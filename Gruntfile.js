module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    bower: {
      dist: {
        rjsConfig: 'assets/js/main.js',
        options: {
          baseUrl: './'
        }
      }
    },
    jshint: {
      files: ['Gruntfile.js', 'src/**/*.js', 'test/**/*.js']
    },
    pure_grids: {
      responsive: {
        dest : 'build/css/vendor/pure/main-grid.scss',
        options: {
            units: 12,
            mediaQueries: {
                sm: 'screen and (min-width: 29em)', // 320px
                md: 'screen and (min-width: 32em)', // 512px
                lg: 'screen and (min-width: 48em)', // 768px
                xl: 'screen and (min-width: 64em)', // 1024px
                xxl: 'screen and (min-width: 80em)' // 1280px
            }
        }
      }
    },
    sass: {
      options: {
        files: [
          {
            expand: true,
            cwd: 'assets/css',
            src: ['**/*.scss'],
            dest: 'build/css',
            ext: '.css'
          }
        ],
        includePaths: [
          'build/css/vendor/pure',
          'build/css/vendor',
          'assets/css'
        ]
      },
      dist: {
        options: {
          outputStyle: 'compressed'
        },
        files: '<%= sass.options.files %>',
        includePaths: '<%= sass.options.includePaths %>'
      },
      dev: {
        options: {
          outputStyle: 'nested'
        },
        files: '<%= sass.options.files %>',
        includePaths: '<%= sass.options.includePaths %>'
      }
    },
    watch: {
      files: ['<%= jshint.files %>'],
      tasks: ['jshint', 'qunit']
    }
  });

  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-bower-requirejs');
  grunt.loadNpmTasks('grunt-sass');
  grunt.loadNpmTasks('grunt-pure-grids');

  grunt.registerTask('install', ['pure_grids','sass:dev']);
  grunt.registerTask('default', ['jshint', 'bower']);
};
