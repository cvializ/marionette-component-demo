define(['underscore', 'backbone'], function (_, Backbone) {
  var NoteModel = Backbone.Model.extend({
    urlRoot: '/notes',
    defaults: {
      author: null,
      title: 'Untitled Note',
      contents: 'Type in some text and take some notes!'
    }
  });

  return NoteModel;
});
