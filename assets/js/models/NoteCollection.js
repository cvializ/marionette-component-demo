define(['backbone', 'models/Note'], function (Backbone, NoteModel) {
  var NoteCollection = Backbone.Collection.extend({
    model: NoteModel,
    url: '/notes'
  });

  return NoteCollection;
});
