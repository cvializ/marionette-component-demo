define(['backbone'], function (Backbone) {
  var AuthorModel = Backbone.Model.extend({
    urlRoot: '/authors',
    defaults: {
      name: 'Unknown Author'
    }
  });

  return AuthorModel;
});
