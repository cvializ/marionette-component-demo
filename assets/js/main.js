require.config({
  paths: {
    backbone: 'bower_components/backbone/backbone',
    'backbone.wreqr': 'bower_components/backbone.wreqr/lib/backbone.wreqr',
    handlebars: 'bower_components/handlebars/handlebars',
    jquery: 'bower_components/jquery/dist/jquery',
    marionette: 'bower_components/marionette/lib/core/backbone.marionette',
    css: 'bower_components/require-css/css',
    'css-builder': 'bower_components/require-css/css-builder',
    normalize: 'bower_components/require-css/normalize',
    requirejs: 'bower_components/requirejs/require',
    underscore: 'bower_components/underscore/underscore',
    hbs: 'bower_components/require-handlebars-plugin/hbs',
    'require-handlebars-plugin': 'bower_components/require-handlebars-plugin/hbs',
    'backbone.babysitter': 'bower_components/backbone.babysitter/lib/backbone.babysitter'
  },
  shim: {
    jquery: {
      exports: 'jQuery'
    },
    underscore: {
      exports: '_'
    },
    backbone: {
      deps: [
        'jquery',
        'underscore'
      ],
      exports: 'Backbone'
    },
    'backbone.wreqr': {
      deps: [
        'backbone'
      ]
    },
    marionette: {
      deps: [
        'jquery',
        'underscore',
        'backbone'
      ],
      exports: 'Marionette'
    }
  },
  hbs: {
    templateExtension: 'html'
  },
  packages: [

  ]
});

define(['DemoApp'], function (DemoApp) {
  DemoApp.start();
});
