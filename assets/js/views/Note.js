define([
  'marionette',
  'vent',
  'models/Note',
  'hbs!templates/note',
  'hbs!templates/editors/note',
  'css!/css/views/note.css'
],
function (Marionette,
          vent,
          NoteModel,
          noteTemplate,
          noteEditTemplate) {
  var NoteView = Marionette.ItemView.extend({

    className: 'd-note-view',

    isEditing: false,

    getTemplate: function () {
      if (this.isEditing) {
        return noteEditTemplate;
      } else {
        return noteTemplate;
      }
    },

    ui: {
      // View template ui elements
      viewContents: '.d-view-contents',
      viewTitle: '.d-view-title',
      delete: '.d-note-delete',

      // Editor template ui elements
      editContents: '.d-edit-contents',
      editTitle: '.d-edit-title',
      save: 'input[value="Save"]',
      cancel: 'input[value="Cancel"]',
    },

    events: {
      'dblclick @ui.viewContents': 'toggleEdit',
      'dblclick @ui.viewTitle': 'toggleEdit',
      'click @ui.delete': 'deleteNote',

      'click @ui.save': 'saveContent',
      'click @ui.cancel': 'toggleEdit',
    },

    // instance methods
    toggleEdit: function () {
      this.isEditing = !this.isEditing;
      this.render();
    },

    saveContent: function () {
      this.model.set('contents', this.ui.editContents.val());
      this.model.set('title', this.ui.editTitle.val());
      this.model.save();

      this.toggleEdit.call(this);
    },

    deleteNote: function () {
      vent.trigger('note:delete', { model: this.model });
    }
  });

  return NoteView;
});
