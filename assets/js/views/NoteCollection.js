define([
  'marionette',
  'jquery',
  'vent',
  'views/Note',
  'hbs!templates/noteCollection',
  'css!/css/views/noteCollection.css'
],
function (Marionette,
          $,
          vent,
          Note,
          noteCollectionTemplate) {

  var NoteCollectionView = Marionette.CompositeView.extend({

    className: 'd-note-collection-view',

    template: noteCollectionTemplate,

    childView: Note,

    childViewContainer: '.d-note-area',

    events: {
      'click .d-add-note': 'addNote'
    },

    addNote: function () {
      vent.trigger('note:new');
    }
  });

  return NoteCollectionView;
});
