define([
  'marionette',
  'vent',
  'mainRegion',
  'controllers/Notes'
],
function (Marionette,
          vent,
          mainRegion,
          NotesController) {
  var DemoApp = new Marionette.Application();

  DemoApp.main = mainRegion;

  DemoApp.addInitializer(function initialize(options) {
    options = options || {};

    this.notesController = new NotesController();

    vent.trigger('notes:initialize');
  });

  return DemoApp;
});
