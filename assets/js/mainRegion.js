define(['marionette'], function (Marionette) {
  var MainRegion = Marionette.Region.extend({
    el: '#demo-main'
  });

  var mainRegion = new MainRegion();

  return mainRegion;
});
