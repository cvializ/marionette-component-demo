define([
  'marionette',
  'vent',
  'mainRegion',
  'models/Note',
  'models/NoteCollection',
  'views/NoteCollection'
],
function (Marionette,
          vent,
          mainRegion,
          NoteModel,
          NoteCollectionModel,
          NoteCollectionView) {

  var NotesController = Marionette.Controller.extend({
    noteCollection: null,

    initialize: function () {
      vent.on('notes:initialize', function () {
        this.noteCollection = new NoteCollectionModel();
        this.noteCollection.fetch();
        
        mainRegion.show(new NoteCollectionView({
          collection: this.noteCollection
        }));
      });

      vent.on('note:new', function () {
        var newNote = new NoteModel({
          title: 'New Note',
          contents: 'Edit to save this note!'
        });

        this.noteCollection.add(newNote);
      });

      vent.on('note:delete', function (data) {
        this.noteCollection.remove(data.model);
        data.model.destroy();
      });
    }
  });

  return NotesController;
});
