var express = require('express'),
    bodyParser = require('body-parser'),
    uuid = require('node-uuid').v4,
    mongoq = require('mongoq'),
    app = express(),
    db = mongoq('demo-notes', {safe: false}),
    server;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false}));
app.use(express.static(__dirname + '/assets'));
app.use(express.static(__dirname + '/build'));
app.use('/js/bower_components', express.static(__dirname + '/bower_components'));

app.post('/notes', function (req, res) {
  var note = req.body;
  note.id = uuid();

  db.collection('notes').insert(note, {safe: true}).done(function(note) {
    res.status(201).json(note);
  });
});

app.get('/notes', function (req, res) {
  db.collection('notes').find().skip(req.query.skip || 0).limit(req.query.limit || 0).toArray().done(function(notes) {
    res.json(notes);
  });
});

app.get('/notes/:id', function(req, res) {
  db.collection('notes').findOne({id: req.params.id}).done(function(note) {
    res.json(note);
  });
});

app.put('/notes/:id', function (req, res) {
  var note = req.body;
  delete note._id; // the backbone _id and mongo _id don't play well

  db.collection('notes').update({id: req.params.id}, {$set: note}, {safe: true}).done(function(success) {
    res.status(success ? 200 : 404).json();
  });
});

app.delete('/notes/:id', function (req, res) {
  db.collection('notes').remove({id: req.params.id}, {safe: true}).done(function(success) {
    res.json(success ? 200 : 404);
  });
});

var server = app.listen(3000, function() {
    console.log('Listening on port %d', server.address().port);
});
