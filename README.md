Marionette and UI Components Demo
====================================

This project demonstrates the ability for widgets and other UI
components to specify their dependencies through RequireJS.

This emulates the principles of encapsulated components, allowing
UI elements to decouple from implementation-specific details and promotes the
development of reusable standards code.

Requirements
------------
- [Node.js](http://nodejs.org/download/)
- [npm](http://nodejs.org/download/) (installed with node)
- [bower](http://bower.io/) manages front-end script dependencies.
- [MongoDB](http://www.mongodb.org/downloads) is used to persist the note data
  across page loads.

On Windows, you may find it helpful to add `%APPDATA%\npm` to the
`PATH` environment variable. This allows Windows to find commands
installed with npm, such as `bower`.

Getting Started
---------------
Make sure you have installed `node`, `bower` and MongoDB.
Ensure that `mongod` is running and is accepting database connections.
If it is not set up to run in the background, you may call it manually.


To install and run this demo project, execute the following commands.

```bash
git clone https://bitbucket.org/cvializ/marionette-component-demo.git
cd marionette-component-demo
npm install # install the project's node dependencies
bower install # install our front-end script dependencies
grunt install # compile sass
node app  # run the server and REST service
```
